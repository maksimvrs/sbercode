from typing import Any

import graphene

from graphql_auth.schema import UserQuery, MeQuery
from user.mutations import Mutation as UserMutation
from content.queries import Query as ContentQuery
from content.mutations import Mutation as ContentMutation
from django.utils.functional import cached_property
from graphene_django.views import GraphQLView

from content.dataloaders import (
    CommentsByPostIdLoader,
    LikesByPostIdLoader,
    ViewsByPostIdLoader,
    CommentsByReplyIdLoader,
    VotesByOfferIdLoader,
    IsLikedByPostIdLoader,
    CommentsCountByPostIdLoader,
)


class GQLContext:
    def __init__(self, request):
        self.request = request

    @cached_property
    def user(self):
        return self.request.user

    @cached_property
    def comments_by_post_id_loader(self):
        return CommentsByPostIdLoader()
    
    @cached_property
    def comments_by_reply_id_loader(self):
        return CommentsByReplyIdLoader()
    
    @cached_property
    def comments_count_by_post_id_loader(self):
        return CommentsCountByPostIdLoader()

    @cached_property
    def votes_by_offer_id_loader(self):
        return VotesByOfferIdLoader()
    
    @cached_property
    def likes_by_post_id_loader(self):
        return LikesByPostIdLoader()
    
    @cached_property
    def liked_by_post_id_loader(self):
        return IsLikedByPostIdLoader(user=self.user)

    @cached_property
    def views_by_post_id_loader(self):
        return ViewsByPostIdLoader()
    
    def __getattr__(self, name: str) -> Any:
        return getattr(self.request, name)


class CustomGraphQLView(GraphQLView):
    def get_context(self, request):
        return GQLContext(request)


class Mutation(
    UserMutation,
    ContentMutation,
    graphene.ObjectType
):
    pass

class Query(
    UserQuery,
    MeQuery,
    ContentQuery,
    graphene.ObjectType
):
    pass


schema = graphene.Schema(query=Query, mutation=Mutation)
