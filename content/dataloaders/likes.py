from django.db.models import Count, Q
from promise import Promise
from promise.dataloader import DataLoader

from content.models import PostModel


class LikesByPostIdLoader(DataLoader):
    def batch_load_fn(self, post_ids):
        likes_count_by_post_ids = dict()
        posts = PostModel.objects.filter(
            Q(pk__in=post_ids) &
            Q(likes__canceled=False)
        ).annotate(likes_count=Count('likes'))
        for post in posts.iterator():
            likes_count_by_post_ids[post.id] = post.likes_count
        return Promise.resolve([
            likes_count_by_post_ids.get(post_id, 0)
            for post_id in post_ids
        ])


class IsLikedByPostIdLoader(DataLoader):
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)

    def batch_load_fn(self, post_ids):
        liked_count_by_post_ids = dict()
        posts = PostModel.objects.filter(
            Q(pk__in=post_ids) &
            Q(likes__author=self.user) &
            Q(likes__canceled=False)
        )
        for post in posts.iterator():
            liked_count_by_post_ids[post.id] = True
        return Promise.resolve([
            liked_count_by_post_ids.get(post_id, False)
            for post_id in post_ids
        ])
