from collections import defaultdict
from django.db.models import Count
from promise import Promise
from promise.dataloader import DataLoader

from content.models import CommentModel, PostModel


class CommentsByPostIdLoader(DataLoader):
    def batch_load_fn(self, post_ids):
        comments_by_post_ids = defaultdict(list)
        comments = CommentModel.objects.filter(
            post_id__in=post_ids
        ).exclude(reply_to__isnull=False)
        for comment in comments.iterator():
            comments_by_post_ids[comment.post_id].append(comment)
        return Promise.resolve([
            comments_by_post_ids.get(post_id, [])
            for post_id in post_ids
        ])


class CommentsByReplyIdLoader(DataLoader):
    def batch_load_fn(self, comments_ids):
        comments_by_reply_ids = defaultdict(list)
        comments = CommentModel.objects.filter(reply_to_id__in=comments_ids)
        for comment in comments.iterator():
            comments_by_reply_ids[comment.reply_to_id].append(comment)
        return Promise.resolve([
            comments_by_reply_ids.get(reply_id, [])
            for reply_id in comments_ids
        ])


class CommentsCountByPostIdLoader(DataLoader):
    def batch_load_fn(self, post_ids):
        comments_count_by_post_ids = dict()
        posts = PostModel.objects.filter(
            pk__in=post_ids
        ).annotate(
            comments_count=Count('comments')
        )
        for post in posts.iterator():
            comments_count_by_post_ids[post.id] = post.comments_count
        return Promise.resolve([
            comments_count_by_post_ids.get(post_id, 0)
            for post_id in post_ids
        ])
