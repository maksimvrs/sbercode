from django.db.models import Count
from promise import Promise
from promise.dataloader import DataLoader

from content.models import PostModel


class ViewsByPostIdLoader(DataLoader):
    def batch_load_fn(self, post_ids):
        views_count_by_post_ids = dict()
        posts = PostModel.objects.filter(
            pk__in=post_ids
        ).annotate(views_count=Count('views'))
        for post in posts.iterator():
            views_count_by_post_ids[post.id] = post.views_count
        return Promise.resolve([
            views_count_by_post_ids.get(post_id, 0)
            for post_id in post_ids
        ])
