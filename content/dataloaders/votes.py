from django.db.models import Count, Q
from promise import Promise
from promise.dataloader import DataLoader

from content.models import (
    Offer as OfferModel,
    VoteType,
)


class VotesByOfferIdLoader(DataLoader):
    def batch_load_fn(self, offer_ids):
        votes_count_by_offer_ids = dict()
        offers = OfferModel.objects.filter(
            pk__in=offer_ids
        ).annotate(
            votes_for=Count('votes', filter=Q(votes__type=VoteType.FOR.value)),
            votes_against=Count('votes', filter=Q(votes__type=VoteType.AGAINST.value)),
        )
        for offer in offers.iterator():
            votes_count_by_offer_ids[offer.id] = (offer.votes_for - offer.votes_against)
        return Promise.resolve([
            votes_count_by_offer_ids.get(offer_id, 0)
            for offer_id in offer_ids
        ])
