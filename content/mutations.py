import graphene

from datetime import datetime
from django.utils.text import slugify
from graphql_jwt.decorators import login_required
from graphql_relay import from_global_id

from .objects import Comment, Post, Offer
from .models import (
    PostModel,
    PostStatus,
    CommentModel,
    LikeModel,
    ViewModel,
    Offer as OfferModel,
    Vote as VoteModel,
    VoteType,
)



class PostInput(graphene.InputObjectType):
    title = graphene.String(required=True)
    content = graphene.String(required=True)
    offer_id = graphene.ID(required=False)


class CreatePost(graphene.Mutation):
    class Arguments:
        post_data = PostInput(required=True)

    ok = graphene.Boolean()
    post = graphene.Field(lambda: Post)

    @login_required
    def mutate(self, info, post_data: PostInput):
        if post_data.offer_id is not None:
            offer = OfferModel.objects.get(pk=from_global_id(post_data.offer_id)[1])
        else:
            offer = None
        post = PostModel(
            title=post_data.title,
            slug=slugify(post_data.title),
            author=info.context.user,
            content=post_data.content,
            offer=offer,
        )
        post.save()
        return CreatePost(post=post, ok=True)


class UpdatePost(graphene.Mutation):
    class Arguments:
        post_id = graphene.ID(required=True)
        post_data = PostInput(required=True)

    ok = graphene.Boolean()
    post = graphene.Field(lambda: Post)

    @login_required
    def mutate(self, info, post_id: str, post_data: PostInput):
        post = PostModel.objects.get(
            pk=from_global_id(post_id)[1],
            author=info.context.user,
        )
        if post_data.offer_id is not None:
            offer = OfferModel.objects.get(pk=from_global_id(post_data.offer_id)[1])
        else:
            offer = None
        post.title = post_data.title
        post.slug = slugify(post_data.title)
        post.content = post_data.content
        post.offer = offer
        post.save()
        return UpdatePost(post=post, ok=True)


class PublishPost(graphene.Mutation):
    class Arguments:
        post_id = graphene.ID(required=True)

    ok = graphene.Boolean()
    post = graphene.Field(lambda: Post)

    @login_required
    def mutate(self, info, post_id: str):
        post = PostModel.objects.get(
            pk=from_global_id(post_id)[1],
            author=info.context.user,
        )
        if post.status == PostStatus.DRAFT.value:
            post.status = PostStatus.MODERATION.value
        elif post.status in [PostStatus.MODERATION.value, PostStatus.PUBLISHED.value]:
            post.status = PostStatus.DRAFT.value
        post.save()
        return PublishPost(post=post, ok=True)


class CommentInput(graphene.InputObjectType):
    post_id = graphene.ID(required=True)
    content = graphene.String(required=True)
    reply_to_id = graphene.ID(required=False)
    

class CreateComment(graphene.Mutation):
    class Arguments:
        comment_data = CommentInput(required=True)

    ok = graphene.Boolean()
    comment = graphene.Field(lambda: Comment)

    @login_required
    def mutate(self, info, comment_data: CommentInput):
        post = PostModel.objects.get(pk=from_global_id(comment_data.post_id)[1])
        reply_to_comment = None
        if comment_data.reply_to_id is not None:
            reply_to_comment = CommentModel.objects.get(pk=from_global_id(comment_data.reply_to_id)[1])
        comment = CommentModel(
            author=info.context.user,
            post=post,
            content=comment_data.content,
            reply_to=reply_to_comment,
        )
        comment.save()
        return CreateComment(comment=comment, ok=True)


class UpdateComment(graphene.Mutation):
    class Arguments:
        comment_id = graphene.ID(required=True)
        content = graphene.String(required=True)

    ok = graphene.Boolean()
    comment = graphene.Field(lambda: Comment)

    @login_required
    def mutate(self, info, comment_id: str, content: str):
        comment = CommentModel.objects.get(
            pk=from_global_id(comment_id)[1],
            author=info.context.user,
        )
        comment.content = content
        comment.save()
        return UpdateComment(comment=comment, ok=True)


class MakeLike(graphene.Mutation):
    class Arguments:
        post_id = graphene.ID(required=True)

    ok = graphene.Boolean()
    post = graphene.Field(lambda: Post)

    @login_required
    def mutate(self, info, post_id: int):
        post = PostModel.objects.get(pk=from_global_id(post_id)[1])
        last_like = LikeModel.objects.filter(
            author=info.context.user,
            post=post,
            canceled=False,
        ).last()
        if last_like is None:
            like = LikeModel(
                author=info.context.user,
                post=post,
            )
            like.save()
        else:
            last_like.canceled = True
            last_like.save()
        return MakeLike(post=post, ok=True)


class OfferInput(graphene.InputObjectType):
    title = graphene.String(required=True)
    

class CreateOffer(graphene.Mutation):
    class Arguments:
        offer_data = OfferInput(required=True)

    ok = graphene.Boolean()
    offer = graphene.Field(lambda: Offer)

    @login_required
    def mutate(self, info, offer_data: OfferInput):
        offer = OfferModel(
            title=offer_data.title,
            author=info.context.user,
        )
        offer.save()
        return CreateOffer(offer=offer, ok=True)


class MakeVote(graphene.Mutation):
    class Arguments:
        offer_id = graphene.ID(required=True)
        type = graphene.Argument(graphene.Enum.from_enum(VoteType), required=True)

    ok = graphene.Boolean()
    offer = graphene.Field(lambda: Offer)

    @login_required
    def mutate(self, info, type: str, offer_id: str):
        offer = OfferModel.objects.get(pk=from_global_id(offer_id)[1])
        vote = VoteModel(
            type=type,
            author=info.context.user,
            offer=offer,
        )
        vote.save()
        return MakeVote(offer=offer, ok=True)
    
    
class TakeOffer(graphene.Mutation):
    class Arguments:
        offer_id = graphene.ID(required=True)

    ok = graphene.Boolean()

    @login_required
    def mutate(self, info, offer_id: str):
        offer = OfferModel.objects.get(pk=from_global_id(offer_id)[1])
        offer.took_to_work = info.context.user
        offer.took_to_work_on = datetime.now()
        offer.save()
        post = PostModel(
            title=offer.title,
            slug=slugify(offer.title),
            author=info.context.user,
            offer=offer,
        )
        post.save()
        return TakeOffer(ok=True)


class Mutation(graphene.ObjectType):
    create_post = CreatePost.Field()
    update_post = UpdatePost.Field()
    publish_post = PublishPost.Field()
    create_comment = CreateComment.Field()
    update_comment = UpdateComment.Field()
    make_like = MakeLike.Field()
    create_offer = CreateOffer.Field()
    make_vote = MakeVote.Field()
    take_offer = TakeOffer.Field()
