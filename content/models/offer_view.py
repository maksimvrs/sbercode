from django.db import models

from user.models import CustomUser

from .offer import Offer


class OfferViewModel(models.Model):
    user = models.ForeignKey(
        CustomUser,
        on_delete=models.CASCADE,
        related_name='offer_views',
    )
    offer = models.ForeignKey(
        Offer,
        on_delete=models.CASCADE,
        related_name='views'
    )
    created_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-created_on']
