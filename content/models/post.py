from enum import Enum

from django.db import models

from user.models import CustomUser

from .offer import Offer


class PostStatus(Enum):
    DRAFT = 'DRAFT'
    MODERATION = 'MODERATION'
    REJECTED = 'REJECTED'
    PUBLISHED = 'PUBLISHED'

    @classmethod
    def choices(cls):
        return tuple((i.name, i.value) for i in cls)


class PostModel(models.Model):
    title = models.CharField(max_length=256, unique=True)
    slug = models.SlugField(max_length=256, unique=True)
    author = models.ForeignKey(
        CustomUser,
        on_delete=models.CASCADE,
        related_name='posts',
    )
    updated_on = models.DateTimeField(auto_now= True)
    content = models.TextField(max_length=(2 ** 20), default='')
    created_on = models.DateTimeField(auto_now_add=True)
    status = models.CharField(
        max_length=16,
        choices=PostStatus.choices(),
        default=PostStatus.DRAFT.value
    )
    offer = models.OneToOneField(
        Offer,
        on_delete=models.CASCADE,
        related_name='post',
        null=True,
    )

    class Meta:
        ordering = ['-created_on']

    def __str__(self):
        return self.title
