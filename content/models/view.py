from django.db import models

from user.models import CustomUser

from .post import PostModel


class ViewModel(models.Model):
    user = models.ForeignKey(
        CustomUser,
        on_delete=models.CASCADE,
        related_name='view',
    )
    post = models.ForeignKey(
        PostModel,
        on_delete=models.CASCADE,
        related_name='views'
    )
    created_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['created_on']
        constraints = [
            models.UniqueConstraint(
                fields=['user', 'post'],
                name='%(app_label)s_%(class)s_unique_view'
            ),
        ]
