from django.db import models

from user.models import CustomUser


class Offer(models.Model):
    author = models.ForeignKey(
        CustomUser,
        on_delete=models.CASCADE,
        related_name='offers',
    )
    title = models.CharField(max_length=256, unique=True)
    updated_on = models.DateTimeField(auto_now=True)
    created_on = models.DateTimeField(auto_now_add=True)
    took_to_work = models.ForeignKey(
        CustomUser,
        on_delete=models.CASCADE,
        related_name='offers_in_work',
        null=True,
    )
    took_to_work_on = models.DateTimeField(null=True)

    class Meta:
        ordering = ['-created_on']

    def __str__(self):
        return self.title
  