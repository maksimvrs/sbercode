from django.db import models

from user.models import CustomUser

from .post import PostModel


class LikeModel(models.Model):
    author = models.ForeignKey(
        CustomUser,
        on_delete=models.CASCADE,
        related_name='likes',
    )
    post = models.ForeignKey(
        PostModel,
        on_delete=models.CASCADE,
        related_name='likes'
    )
    canceled = models.BooleanField(default=False)
    updated_on = models.DateTimeField(auto_now=True)
    created_on = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['created_on']
        constraints = [
            models.UniqueConstraint(
                fields=['author', 'post'],
                condition=models.Q(canceled=False), name='%(app_label)s_%(class)s_unique_like'
            ),
        ]

    def __str__(self):
        return 'CANCELED_LIKE' if self.canceled else 'LIKE'
