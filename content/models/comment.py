from django.db import models

from user.models import CustomUser

from .post import PostModel


class CommentModel(models.Model):
    author = models.ForeignKey(
        CustomUser,
        on_delete=models.CASCADE,
        related_name='comments',
    )
    post = models.ForeignKey(
        PostModel,
        on_delete=models.CASCADE,
        related_name='comments',
    )
    updated_on = models.DateTimeField(auto_now=True)
    content = models.TextField(max_length=4096)
    created_on = models.DateTimeField(auto_now_add=True)
    reply_to = models.ForeignKey(
        'self',
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name='replies'
    )

    class Meta:
        ordering = ['created_on']

    def __str__(self):
        return self.content
