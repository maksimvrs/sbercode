from enum import Enum

from django.db import models

from user.models import CustomUser

from .offer import Offer


class VoteType(Enum):
    FOR = 'FOR'
    AGAINST = 'AGAINST'
    PASS = 'PASS'

    @classmethod
    def choices(cls):
        return tuple((i.value, i.name) for i in cls)


class Vote(models.Model):
    author = models.ForeignKey(
        CustomUser,
        on_delete=models.CASCADE,
        related_name='votes',
    )
    created_on = models.DateTimeField(auto_now_add=True)
    type = models.CharField(
        max_length=16,
        choices=VoteType.choices(),
    )

    offer = models.ForeignKey(
        Offer,
        on_delete=models.CASCADE,
        related_name='votes',
        null=True,
    )

    class Meta:
        ordering = ['created_on']
        constraints = [
            models.UniqueConstraint(
                fields=['author', 'offer'],
                name='%(app_label)s_%(class)s_unique_vote'
            ),
        ]

    def __str__(self):
        return self.type
  