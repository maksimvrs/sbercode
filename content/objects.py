import graphene

import django_filters
from django.db.models import Q
from graphene import relay
from graphene_django import DjangoObjectType
from graphql_auth.schema import UserNode

from .models import (
    PostModel,
    CommentModel,
    LikeModel,
    ViewModel,
    Offer as OfferModel,
    PostStatus,
)


class Card(graphene.Interface):
    class Meta:
        interfaces = (graphene.relay.Node, )

    id = graphene.ID(required=True)


class Comment(DjangoObjectType):
    class Meta:
        interfaces = (graphene.relay.Node, )
        model = CommentModel

    def resolve_replies(self, info, **kwargs):
        return info.context.comments_by_reply_id_loader.load(self.id)


class CommentsConnection(relay.Connection):
    class Meta:
        node = Comment


class PostFilter(django_filters.FilterSet):
    search = django_filters.CharFilter(method='search_filter')
    status = django_filters.MultipleChoiceFilter(choices=PostStatus.choices(), method='status_filter')

    class Meta:
        model = PostModel
        fields = ['search', 'status', 'author']
        
    def search_filter(self, queryset, name, value):
        return queryset.filter(
            Q(title__icontains=value) | Q(content__icontains=value)
        )

    def status_filter(self, queryset, name, value):
        return queryset.filter(status__in=value)


class Post(DjangoObjectType):
    class Meta:
        interfaces = (Card, graphene.relay.Node, )
        model = PostModel
        filterset_class = PostFilter

    views = graphene.Int(required=True)
    likes = graphene.Int(required=True)
    comments_count = graphene.Int(required=True)
    is_liked = graphene.Boolean(required=True)
    author = graphene.Field(UserNode, required=True)

    def resolve_comments(self, info, **kwargs):
        return info.context.comments_by_post_id_loader.load(self.id)

    def resolve_likes(self, info):
        return info.context.likes_by_post_id_loader.load(self.id)

    def resolve_comments_count(self, info):
        return info.context.comments_count_by_post_id_loader.load(self.id)
    
    def resolve_is_liked(self, info):
        return info.context.liked_by_post_id_loader.load(self.id)

    def resolve_views(self, info):
        return info.context.views_by_post_id_loader.load(self.id)


class Vote(graphene.ObjectType):
    class Meta:
        interfaces = (Card, )
        title = graphene.String(required=True)


class OfferFilter(django_filters.FilterSet):
    class Meta:
        model = OfferModel
        fields = ['title']


class Offer(DjangoObjectType):
    class Meta:
        interfaces = (Card, graphene.relay.Node, )
        model = OfferModel
        filterset_class = OfferFilter

    votes = graphene.Int(required=True)
    
    def resolve_votes(self, info, **kwargs):
        return info.context.votes_by_offer_id_loader.load(self.id)


class CardConnection(relay.Connection):
    class Meta:
        node = Card


class Statistics(graphene.ObjectType):
    distance = graphene.String(required=True)
    
class UserInfo(graphene.ObjectType):
    balance = graphene.Int(required=True)
