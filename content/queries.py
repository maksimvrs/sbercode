from datetime import datetime
import graphene

from decimal import Decimal, ROUND_HALF_UP
from django.conf import settings
from django.utils import timezone
from django.db.models import Q, Max, Sum, Count
from django.db.models.functions import Length
from django.db import transaction
from graphql_jwt.decorators import login_required
from graphene_django.filter import DjangoFilterConnectionField
from graphql_relay import from_global_id

from .objects import Card, Post, Vote, Offer, Statistics, UserInfo
from .models import (
    PostModel,
    Offer as OfferModel,
    OfferViewModel,
    ViewModel,
    VoteType,
    PostStatus,
)


class Query(graphene.ObjectType):
    all_posts = DjangoFilterConnectionField(Post)
    my_posts = DjangoFilterConnectionField(Post)
    get_post = graphene.Field(Post, id=graphene.ID(required=True))
    offers_ideas = DjangoFilterConnectionField(Offer)
    my_offers = DjangoFilterConnectionField(Offer)
    my_offer_notification = graphene.Field(Offer)
    get_next_offer = graphene.Field(Offer)
    statistics = graphene.Field(Statistics)
    user_info = graphene.Field(UserInfo)

    @login_required
    def resolve_all_posts(self, info, **kwargs):
        return PostModel.objects.filter(
            ~Q(author=info.context.user) &
            Q(status=PostStatus.PUBLISHED.value)
        ).all()

    @login_required
    def resolve_my_posts(self, info, **kwargs):
        return PostModel.objects.filter(author=info.context.user).all()
    
    @login_required
    def resolve_get_post(self, info, id: str, **kwargs):
        post = PostModel.objects.get(pk=from_global_id(id)[1])
        try:
            ViewModel.objects.get_or_create(
                user=info.context.user,
                post=post,
            )
        except Exception:
            pass
        return post

    @login_required
    def resolve_offers_ideas(self, info, **kwargs):
        return OfferModel.objects.filter(took_to_work__isnull=True).all()

    @login_required
    def resolve_my_offers(self, info, **kwargs):
        return OfferModel.objects.annotate(
            votes_value=(
                Count('votes', filter=Q(votes__type=VoteType.FOR.value)) -
                Count('votes', filter=Q(votes__type=VoteType.AGAINST.value))
            )
        ).filter(
            Q(took_to_work=info.context.user) &
            Q(votes_value__gt=0)
        ).order_by('-votes_value').all()

    @login_required
    def resolve_my_offer_notification(self, info, **kwargs):
        last_shown_offer = OfferModel.objects.annotate(
            last_view=Max('views__created_on', filter=Q(views__user=info.context.user))
        ).filter(took_to_work=info.context.user).order_by('-last_view').first()
        
        if (
            last_shown_offer is not None and
            last_shown_offer.last_view is not None and
            last_shown_offer.last_view + settings.OFFER_NOTIFICATION_TIMEOUT > timezone.now()
        ):
            return None

        offer = OfferModel.objects.annotate(
            votes_value=(
                Count('votes', filter=Q(votes__type=VoteType.FOR.value)) -
                Count('votes', filter=Q(votes__type=VoteType.AGAINST.value))
            ),
            last_view=Max('views__created_on', filter=Q(views__user=info.context.user))
        ).filter(
            Q(took_to_work=info.context.user) &
            Q(votes_value__gt=0)
        ).order_by('last_view', '-votes_value').first()
        try:
            OfferViewModel.objects.create(
                user=info.context.user,
                offer=offer,
            )
        except Exception:
            pass
        return offer

    @login_required
    def resolve_get_next_offer(self, info, **kwargs):
        offer = OfferModel.objects.annotate(
            last_view=Max('views__created_on', filter=Q(views__user=info.context.user))
        ).filter(
            ~Q(took_to_work=info.context.user)
        ).order_by('last_view', '-created_on').first()
        if offer is not None:
            offer_view = OfferViewModel(
                user=info.context.user,
                offer=offer,
            )
            offer_view.save()
        return offer

    @login_required
    def resolve_statistics(self, info, **kwargs):
        symbols_count = ViewModel.objects.annotate(
            symbols_count=Sum(Length('post__content'))
        ).filter(
            user=info.context.user,
        ).aggregate(Sum('symbols_count')).get('symbols_count__sum', 0.00)
        distance = (settings.M_IN_PX * settings.PX_IN_SYMBOL * symbols_count)
        return Statistics(
            distance=Decimal(distance.quantize(Decimal('.01'), rounding=ROUND_HALF_UP))
        )

    @login_required
    def resolve_user_info(self, info, **kwargs):
        user_posts_count = PostModel.objects.filter(author=info.context.user).count()
        read_count = ViewModel.objects.filter(user=info.context.user).count()
        return UserInfo(
            balance=(
                user_posts_count * settings.MONEY_FOR_WRITE +
                read_count * settings.MONEY_FOR_READ
            )
        )
